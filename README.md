# Picotron Laptop Bridge

Picotron Laptop Bridge, or PLB, is an application that bridges the gap between Picotron and a laptop system. It will probably work fine on other types of computers too. Currently Debian 12 and Windows is supported.

* Some current features:
 * Monitor's Picotron status
 * GUI tool when Picotron is closed to reload, reboot, shutdown, or open a command prompt/terminal window.
 * Simple API system
 * Battery polling

This project is released under the GPLv3 license.

#### To install:

\*Note that this guide uses sudo throughout and in some places does not use sudo. To make sure you have setup the proper permissions, make sure you are following this guide and using sudo appropriately. It is not recommended to "sudo su" your way around it, as far as this guide is concerned.

* Install debian; install a minimum system without a desktop, make sure you have created a user account during setup.
* Install any drivers you might need and otherwise configure your base system to your liking. Here’s an example for installing a lot of drivers:

```bash
sudo apt install firmware-linux firmware-linux-nonfree firmware-misc-linux firmware-intel-sound firmware-sof-signed live-task-non-free-firmware-pc
```

* Install sound, battery utility, and build tools

```bash
sudo apt install pavucontrol acpi linux-headers-$(uname -r) build-essential
```

* Install the window manager we will use, zip for unzipping picotron, and xterm for working under the hood

```bash
sudo apt install lightdm openbox zip xterm
```

* Allow your user to shutdown the system, make the file if needed

```bash
sudo nano /etc/sudoers.d/reboot
```

* Add the contents:

```bash
%wheel ALL= NOPASSWD: /sbin/shutdown /sbin/reboot
```

* Configure lightdm to autoboot

```bash
sudo nano /etc/lightdm/lightdm.conf
```

* Add to the bottom:

```txt
[SeatDefaults]
autologin-user=YourUsername
user-session=openbox
```

* **Next we install Picotron and make it available for all users.** *We won't be putting picotron in the /home directory because we'll be mounting the home directory inside of Picotron and we don't want to be messing with the binary inside of Picotron.*
* Load Picotron zip file to your system, you'll have to do this on your own

```bash
unzip picotronN.N.NZamd64.zip
sudo mv picotron /opt/picotron
sudo chown root:root /opt/picotron -R
sudo ln -s /opt/picotron/picotron /usr/local/bin/picotron
```

* Configure openbox to use PLB

```bash
cd
mkdir .config && mkdir .config/openbox
nano .config/openbox/autostart
```

* Contents of file should be only the one line:

```txt
plb &
```

* Next we update some settings in the Picotron config to give us access to the /home folder and install the Picotron apps that come with PLB:

```bash
nano ~/.lexaloffle/Picotron/picotron_config.txt
```

* If the file does not exist, you can create it, if it does exist, you can overwrite/add to it:

```txt
# picotron config
# mount / /home/username/.lexaloffle/Picotron/drive \<-- comment this out
mount / /home/username
mount /plb_readonly /opt/picotron-laptop-bridge/picotron-apps
```

* Be aware that the plb folder is read only, so when you save, the changes are not actually saved to disk. You should copy it to another folder

* Next up we add the startup entries, create it if it's not there
```bash 
cat /opt/picotron-laptop-bridge/picotron-apps/startup.lua >> ~/.lexaloffle/Picotron/drive/appdata/system/startup.lua
```

* Now we will install Picotron Laptop Bridge

```bash
sudo apt install git
cd /opt
sudo git clone https://gitlab.com/trs-eric/picotron-laptop-bridge.git
sudo ln -s /opt/picotron-laptop-bridge/output/plb /usr/local/bin/plb
```

* Reboot your computer and it should load into Picotron!

* To update PLB:
```bash
cd /opt/picotron-laptop-bridge
sudo git pull
```